import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Rating as Star } from '@mui/material';

import Restaurant from "../interfaces/Restaurant"

interface Props {
    restaurant: Restaurant

}

const CardStyle = { display: 'flex', justifyContent: "space-between", marginTop: "5%", marginBottom: "5%", border: "1px solid #1976d2" }

const CostumCard = ({ restaurant }: Props) => {
    const {
        LogoUrl,
        Rating,
        Name } = restaurant

    const normalizedRanking: number = Math.round(Rating.StarRating * 0.5 * 10) / 10

    return (
        <Card sx={CardStyle}>
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <CardContent sx={{ flex: '1 0 auto' }}>
                    <Typography component="div" variant="h5">
                        {Name}
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary" component="div" sx={{ display: "inline-flex" }}>
                        {normalizedRanking}
                        <Star name="read-only" precision={0.1} value={normalizedRanking} max={5} readOnly sx={{ margin: "0.2%" }} />
                    </Typography>
                </CardContent>
            </Box>
            <CardMedia
                component="img"
                sx={{ maxWidth: 151, maxHeight: "100%", padding: "0.2%" }}
                image={LogoUrl}
                alt={Name}
            />
        </Card>
    );
}

export default CostumCard 