import React, { useState } from "react";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

interface Props {
  nPages: number,
  nElements: number,
  index: Function
}


const CostumPagination = (props: Props) => {
  const { nElements, index } = props
  const [page, setPage] = useState(1);

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value)
    index(value)
    window.scroll(0,0)
  };

  return (
    <Stack justifyContent="center"
      alignItems="center"
      marginY={"2%"}
      spacing={2}>
      <Pagination size="large" count={nElements} page={page} onChange={handleChange} />
    </Stack>
  );
}

export default CostumPagination