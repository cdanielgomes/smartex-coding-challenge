import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

interface Props {
    clickCallback: Function;
    buttonName: string,
    inputPlaceholder: string,

}

const SearchMechanism = ({ clickCallback, buttonName, inputPlaceholder }: Props) => {

    const [inputValue, setInputValue] = useState<string>("")

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.preventDefault()
        setInputValue(e.target.value)
    }

    const handleClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
        clickCallback(inputValue)(e)

    }
    return (<Stack spacing={5} direction="row" sx={{justifyContent:"center", marginTop:"2%"}}>
        <TextField sx={{width:"50%"}} id="standard-basic" label={inputPlaceholder} onChange={handleInputChange} variant="standard" />
        <Button size="large" onClick={handleClick} variant="outlined"> {buttonName} </Button>
    </Stack>)
}

export default SearchMechanism;