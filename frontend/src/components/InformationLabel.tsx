import React from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

interface Props {
    message: string
}

const InformationLabel = (props: Props) => {

    return <Box >
        <Typography sx={{ display: "flex", justifyContent:"center", fontSize:"bold", marginY:"2%"}} component="div" variant="h5">
            {props.message}
        </Typography>
    </Box>
}

export default InformationLabel