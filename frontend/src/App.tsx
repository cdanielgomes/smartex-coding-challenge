import React from 'react';

import FindRestaurant from "./pages/FindRestaurant"
import { Container } from "@mui/material"

function App() {
  return <>
    <Container>
      <FindRestaurant elemPerPager={10}/>
    </Container>
  </>
}

export default App;
