import Rating from "./Rating"

interface Restaurant {
    IsOpenNow: boolean,
    LogoUrl: string,
    Rating: Rating,
    Name: string
}

export default Restaurant