const divideArray = (items: Array<any>, quantity: number) => {
    const chunks = []
    items = [].concat(...items)

    while (items.length) {
        chunks.push(
            items.splice(0, quantity)
        )
    }

    return chunks
}

export default divideArray