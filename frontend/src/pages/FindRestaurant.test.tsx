/* eslint-disable testing-library/no-debugging-utils */
import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import FindRestaurant from "./FindRestaurant"
import restOpen from "../testAssets/restOpen"
import restClosed from "../testAssets/restClosed"

const setup = () => {
    const utils = render(<FindRestaurant elemPerPager={10} />)
    const button = screen.getByRole('button')
    const input = screen.getByLabelText('Outpost')
    const text = screen.getByText(/Nothing to find/)
    return {
        button,
        input,
        text,
        ...utils,
    }
}

const urlToCatch ='http://localhost:8080/:id'

const server = setupServer(
    rest.get(urlToCatch, (req, res, ctx) => {
        return res(ctx.json({ Restaurants: restOpen }))
    }),
)


beforeAll(() => server.listen({ onUnhandledRequest: "bypass" }))
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('Initial setup correct', () => {
    const { button, input, text } = setup()

    expect(button).toBeDefined()
    expect(input).toBeDefined()
    expect(text).toBeDefined()

})

test('Expect to render 5 restaurants', async () => {
    const { container, button, input } = setup()
    const value = 'ec4m'

    fireEvent.change(input, { target: { value: value } })

    expect(screen.getByDisplayValue(value) === input).toBe(true)

    fireEvent.click(button)
    
    const nText = screen.queryByText(/Nothing to find/)
    expect(nText).toBeNull()

    // eslint-disable-next-line testing-library/no-node-access
    const loading = container.querySelector("[role=progressbar]")
    
    expect(loading).toBeDefined()

    const pagination = await screen.findByLabelText("pagination navigation")

    expect(pagination).toBeDefined()

    const cards = screen.getByTestId("cards")

    expect(cards).toBeDefined()
    expect(cards.childNodes.length).toBe(5)

})

test('Expect a good call but no render because restaurants are close', async () => {
    const { container, button, input } = setup()
    const value = 'ec4m'
    server.use(
        rest.get(urlToCatch, (req, res, ctx) => {
            return res(ctx.json({ Restaurants: restClosed }))
        }),
    )
    fireEvent.change(input, { target: { value: value } })

    expect(screen.getByDisplayValue(value) === input).toBe(true)

    fireEvent.click(button)
    
    const nText = screen.queryByText(/Nothing to find/)
    expect(nText).toBeNull()

    // eslint-disable-next-line testing-library/no-node-access
    const loading = container.querySelector("[role=progressbar]")
    
    expect(loading).toBeDefined()

    await screen.findByText(/Nothing to find/)
    
    expect(screen.getByText(/Nothing to find/)).toBeDefined()
})

test('Try to get restaurants but return error', async () => {
    const { button, input } = setup()
   
    const value = '32/6' //not found by the app

    fireEvent.change(input, { target: { value: value } })
    expect(screen.getByDisplayValue(value) === input).toBe(true)

    fireEvent.click(button)

    const error = await screen.findByText("Error Happen! Search again please")

    expect(error).toBeDefined()


})