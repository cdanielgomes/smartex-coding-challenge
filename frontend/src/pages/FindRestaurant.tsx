import React, { useState } from "react";
import getRestaurants from "../services/getRestaurants";
import SearchMechanism from "../components/SearchMechanism"
import CostumPagination from "../components/CostumPagination"
import CostumCard from "../components/CostumCard"
import InformationLabel from "../components/InformationLabel"

import CircularProgress from "../components/CircularIndeterminate"

import Restaurant from "../interfaces/Restaurant"
import divideArray from "../helpers/util"


interface Props {
    elemPerPager: number
}

const FindRestaurant = (props: Props) => {
    const [restaurants, setRestaurants] = useState<Array<Array<Restaurant>>>([])
    const [index, setIndex] = useState<number>(1)

    const [loading, setLoading] = useState<boolean>(false)
    const [error, setError] = useState<boolean>(false)

    const handleClick = (inputValue: string) => {
        return (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            e.preventDefault()
            e.stopPropagation()

            getRestaurants(inputValue)
                .then((info) => {
                    const { Restaurants } = info
                    const restsOpen = Restaurants.filter((elem: Restaurant) => elem.IsOpenNow)
                    setRestaurants(divideArray(restsOpen, props.elemPerPager))
                    setLoading(false)
                    setError(false)

                })
                .catch(error => {
                    setError(error)
                    setLoading(false)
                    setRestaurants([])
                })

            setLoading(true)
        }
    }


    const showResults = () => {
        return <>
            <div data-testid="cards">{restaurants[index - 1].map((x, i) => <CostumCard key={i} restaurant={x} />)}</div>
            <CostumPagination nPages={props.elemPerPager} nElements={restaurants.length} index={setIndex} />
        </>
    }

    return <div>
        <SearchMechanism clickCallback={handleClick} buttonName="Search" inputPlaceholder="Outpost" />

        {loading ?
            <CircularProgress /> : error ?
                <InformationLabel message="Error Happen! Search again please" /> : restaurants.length ?
                    showResults() : <InformationLabel message="Nothing to find" />}


    </div>

}

export default FindRestaurant