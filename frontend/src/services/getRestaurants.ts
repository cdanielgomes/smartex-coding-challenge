const baseURL: string = "http://localhost:8080"

const getRestaurants = async (id: string) => {

    const url: string = `${baseURL}/${id}`
    try {
        const response = await fetch(url,
            {
                method: 'GET',
                headers: { "Access-Control-Allow-Origin": "*" }
            }).catch((e) => e)


        if (!response.ok) {
            return Promise.reject()
        }
        const restaurants = response.json()

        return restaurants
    } catch (error) {
        return Promise.reject(error)
    }
    
    

}

export default getRestaurants