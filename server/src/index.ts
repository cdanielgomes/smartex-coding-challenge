import express from "express";
import axios from "axios";
import cors from "cors";

const app = express();
const port = 8080; // default port to listen
app.use(cors())
// define a route handler for the default home page
app.get("/:id", (req, res) => {
    const url: string = `https://uk.api.just-eat.io/restaurants/bypostcode/${req.params.id}`
    axios({
        method: "get",
        url,
    })
        .then(({data}) => {
            res.json(data)
        })
        .catch((error) => {
             // tslint:disable-next-line:no-console
            res.send(error)
        })
});

app.get("*", (req, res) => {

    res.send("PAGE NOT FOUND")
  });

// start the Express server
app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${port}`);
});