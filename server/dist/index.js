"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const axios_1 = __importDefault(require("axios"));
const cors_1 = __importDefault(require("cors"));
const app = (0, express_1.default)();
const port = 8080; // default port to listen
app.use((0, cors_1.default)());
// define a route handler for the default home page
app.get("/:id", (req, res) => {
    const url = `https://uk.api.just-eat.io/restaurants/bypostcode/${req.params.id}`;
    (0, axios_1.default)({
        method: "get",
        url,
    })
        .then(({ data }) => {
        res.json(data);
    })
        .catch((error) => {
        // tslint:disable-next-line:no-console
        res.send(error);
    });
});
app.get("*", (req, res) => {
    res.send("PAGE NOT FOUND");
});
// start the Express server
app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map