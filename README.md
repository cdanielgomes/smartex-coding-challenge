# Smartex Coding Challenge

This project is divided in 2 sections: frontend and server,  where the server is a simple proxy to the Just Eat API. 

## Build and Run instructions

- go to folder `server` and run `npm install`
- then, go to folder `frontend` and run `npm install`
- be free to play with the client

## Tests

The frontend tests are not running automatically. To verify them run `npm run test` inside frontend project

## Considerations 

Be sure that you have the ports 3000 and 8080 free. I am aware that hardcoded ports are a bad bad practice but I do not have the time neither availability to refactor this part

## User Stories implemented

- [x] Given I am a **user running the application**
When I **submit an outcode (e.g. SE19)**
Then I want to see a **list of restaurants**
And I only want to see **restaurants that are currently open**


