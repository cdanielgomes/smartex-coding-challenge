1. **How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.** 

I spent 2 days in the coding challenge, I had to learn the minimal about typescript, to remember everything about frontend testing and to set up the proxy (this was the easy part actually). 

For my mistake, I didn't notice the the "Cuisine Types" were mandatory and now I do not have the time to code that little component. Even the stars rating is there because I had some fun doing it and I like to see this feature in a perspective of user experience.


 
2. **What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.**

I have no idea! I had to learn the basics of typescript to be capable of doing the challenge. I searched about the lastest version features of TS but I believe I didn't use none of them. 

2. **How would you track down a performance issue in production? Have you ever had to do this?**

I've never done this before.